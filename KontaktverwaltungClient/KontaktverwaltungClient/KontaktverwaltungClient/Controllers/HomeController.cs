﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KontaktverwaltungClient.Models;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace KontaktverwaltungClient.Controllers
{
    public class HomeController : Controller
    {
        HttpClient anfrage;

        public HomeController()
        {
            anfrage = new HttpClient();
            anfrage.BaseAddress = new Uri("http://localhost:57403/");
            anfrage.DefaultRequestHeaders.Clear();
        }

        public IActionResult Index()
        {
            return View();
        }
        //------------------------------------------------------------------------------

        //Für Create
        public IActionResult Create()
        {
            return View();
        }


        // Create für Posten
        [HttpPost]
        public async Task<IActionResult> Create(string vorname, string nachname, string plz)
        {
            string jsonString = string.Format("Vorname:'{0}',Nachname:'{1}',plz:'{2}'", vorname, nachname, plz);

            jsonString = "{" + jsonString + "}";

            StringContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            HttpResponseMessage responseMessage = await anfrage.PostAsync("api/values/create/", content);


            return View();
        }

        // Update  Update  Update  Update  Update  Update  Update  Update  Update  Update
        public IActionResult Update()
        {
            HttpResponseMessage responseMessage = anfrage.GetAsync("api/values/getAll").Result;
            string result = responseMessage.Content.ReadAsStringAsync().Result;
            List<Kontakte> alleKontakte = JsonConvert.DeserializeObject<List<Kontakte>>(result);
            ViewBag.Liste = new SelectList(alleKontakte, "Vorname", "Vorname");


            return View();
        }

        //Update Methode
        [HttpPost]
        public IActionResult UpdateAnzeigen(string Liste)
        {
            HttpResponseMessage responseMessage = anfrage.GetAsync("api/values/getOne/"+Liste).Result;
            string result = responseMessage.Content.ReadAsStringAsync().Result;

            Kontakte kon = JsonConvert.DeserializeObject<Kontakte>(result);

            return View(kon);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Kontakte kon)
        {

            string jsonString = JsonConvert.SerializeObject(kon);

            StringContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            HttpResponseMessage responseMessage = await anfrage.PutAsync("api/values/update/", content);

           

            return View("Index");
        }


        //------------------------------------------------------------------------------

        // Delete  Delete  Delete  Delete  Delete Delete  Delete  Delete  Delete  Delete
        public IActionResult Delete()
        {
            HttpResponseMessage responseMessage = anfrage.GetAsync("api/values/getAll").Result;
            string result = responseMessage.Content.ReadAsStringAsync().Result;
            List<Kontakte> alleKontakte = JsonConvert.DeserializeObject<List<Kontakte>>(result);
            ViewBag.Liste = new SelectList(alleKontakte, "Vorname", "Vorname");

            return View();
        }

        // Delete Methode fürs Posten
        [HttpPost]
        public async Task<IActionResult> Delete(string Liste)
        {
            HttpResponseMessage responseMessage = await anfrage.DeleteAsync("api/values/delete/"+Liste);

            return View("Index");
        }

        //--------------------------------------------------------------------------

        // Alle Kontakte  Alle Kontakte  Alle Kontakte  Alle Kontakte  Alle Kontakte
        public IActionResult AlleAnzeigen()
        {
            HttpResponseMessage responseMessage = anfrage.GetAsync("api/values/getAll").Result;
            string result = responseMessage.Content.ReadAsStringAsync().Result;
            List<Kontakte> alleKontakte = JsonConvert.DeserializeObject<List<Kontakte>>(result);


            return View(alleKontakte);
        }

        // Alle Kontakte Anzeigen fürs Posten
        [HttpPost]
        public IActionResult AlleAnzeigen(int Id)
        {
            return View();
        }

        //----------------------------------------------------------------------------
        // Error  Error  Error  Error  Error  Error  Error  Error  Error  Error  Error 
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

