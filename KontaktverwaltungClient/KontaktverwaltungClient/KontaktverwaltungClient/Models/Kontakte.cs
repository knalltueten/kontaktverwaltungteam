﻿using System;
using System.Collections.Generic;

namespace KontaktverwaltungClient.Models
{
    public partial class Kontakte
    {
        public int Id { get; set; }
        public string Geschlecht { get; set; }
        public string Nachname { get; set; }
        public string Ort { get; set; }
        public string Plz { get; set; }
        public string Vorname { get; set; }
    }
}
