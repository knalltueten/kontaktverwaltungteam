﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KontaktverwaltungTeam.Models
{
    public class Kontakt
    {
        // Attribute
        public int ID { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string PLZ { get; set; }
        public string Ort { get; set; }
        public string Geschlecht { get; set; }

        
    }
}
