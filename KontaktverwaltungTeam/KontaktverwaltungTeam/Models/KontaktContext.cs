﻿using KontaktverwaltungTeam.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KontaktverwaltungTeam.Controllers
{
    public class KontaktContext :DbContext
    {
        // 

        public virtual DbSet<Kontakt> Kontakte { get; set; }

        //Konstrukor

        public KontaktContext(DbContextOptions<KontaktContext> options)
                                : base(options) { }


    }
}
