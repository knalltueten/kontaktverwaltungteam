﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using KontaktverwaltungTeam.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace KontaktverwaltungTeam.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        KontaktContext _context;
        HttpClient genderize;
        HttpClient zip;

        //Konstuktor
        public ValuesController(KontaktContext context)
        {
            _context = context;


            //Gender Verbindung
            genderize = new HttpClient();
            genderize.BaseAddress = new Uri("https://api.genderize.io/");
            genderize.DefaultRequestHeaders.Clear();

            //PLZVerbindung
            zip = new HttpClient();
            zip.BaseAddress = new Uri("http://api.zippopotam.us/");
            zip.DefaultRequestHeaders.Clear();
        }



        //Methoden
        [HttpPost("create")]
        public IEnumerable<string> Create([FromBody] Kontakt neuerKontakt)
        {
            //Gender ermitteln
            HttpResponseMessage responseMessage = genderize.GetAsync("?name=" + neuerKontakt.Vorname).Result;
            string gender = responseMessage.Content.ReadAsStringAsync().Result;
            string[] genderSplit = gender.Split('\"', StringSplitOptions.RemoveEmptyEntries);
            if (genderSplit.Length >=8 )
            {
                neuerKontakt.Geschlecht = genderSplit[7];
            }
            
            //7.

            // Convert.ToInt32(neuerKontakt.PLZ)
            HttpResponseMessage responseMessage2 = zip.GetAsync("de/"+neuerKontakt.PLZ).Result;
            string zipString = responseMessage2.Content.ReadAsStringAsync().Result;
            string[] zipSplit = zipString.Split('\"',StringSplitOptions.RemoveEmptyEntries);
            if (zipSplit.Length >15)
            {
                neuerKontakt.Ort = zipSplit[17];
            }
            //17.

            _context.Kontakte.Add(neuerKontakt);
            _context.SaveChanges();


            return null;
        }


        //GetAlleKontakte
        [HttpGet("getAll")]
        public IEnumerable<Kontakt> GetAll()
        {
            List<Kontakt> alleKontakte = _context.Kontakte.ToList();

            return alleKontakte;
        }


        [HttpGet("getOne/{vorname}")]
        public Kontakt GetOne(string vorname)
        {
            Kontakt einKontakt = _context.Kontakte.Where(x=>x.Vorname == vorname).SingleOrDefault();

            return einKontakt;
        }

        //Update
        [HttpPut("update")]
        public IEnumerable<Kontakt> Update([FromBody] Kontakt neuerKontakt)
        {
            Kontakt updateKontakt = neuerKontakt;


            _context.Update(updateKontakt);

            _context.SaveChanges();

            return null;
        }


        //Delete
        [HttpDelete("delete/{vorname}")]
        public IEnumerable<Kontakt> Delete(string vorname)
        {

            Kontakt deleteIt = _context.Kontakte.Where(x => x.Vorname == vorname).SingleOrDefault();
            _context.Kontakte.Remove(deleteIt);
            _context.SaveChanges();

            return null;
        }
























        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        //bla

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}



